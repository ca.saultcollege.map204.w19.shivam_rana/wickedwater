package com.example.a18035030.wickedwater;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateAccountActivity extends AppCompatActivity {
    EditText firstnameCreateAccountEditText,lastnameCreateAccountEditText,emailCreateAccountEditText,passwordCreateAccountEditText,confirrmPasswordCreateAccountEditText,phoneCreateAccountEditText;
    Button signUpCreateAccountButton,cancelCreateAccountButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        firstnameCreateAccountEditText = (EditText) findViewById(R.id.firstNameCreateAccountEditText);
        lastnameCreateAccountEditText = (EditText) findViewById(R.id.lastNameCreateAccountEditText);
        emailCreateAccountEditText = (EditText) findViewById(R.id.emailCreateAccountEditText);
        passwordCreateAccountEditText = (EditText) findViewById(R.id.passwordCreateAccountEditText);
        confirrmPasswordCreateAccountEditText = (EditText) findViewById(R.id.confirmPasswordCreateAccountEditText);
        phoneCreateAccountEditText = (EditText) findViewById(R.id.phoneCreateAccountButton);
        signUpCreateAccountButton = (Button) findViewById(R.id.signupCreateAccountButton);
        cancelCreateAccountButton = (Button) findViewById(R.id.cancelCreateAccountButton);

        signUpCreateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String getFirstname = firstnameCreateAccountEditText.getText().toString();
                String getLastname = lastnameCreateAccountEditText.getText().toString();
                String getEmail = emailCreateAccountEditText.getText().toString();
                String getPassword = lastnameCreateAccountEditText.getText().toString();
                String getConfirmPassword = lastnameCreateAccountEditText.getText().toString();
                String getPhone = lastnameCreateAccountEditText.getText().toString();

                if(getFirstname.equals("") || getLastname.equals("") || getEmail.equals("") || getPassword.equals("") || getConfirmPassword.equals("") || getPhone.equals("") ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccountActivity.this);
                    builder.setMessage("Please enter the data");
                    builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            firstnameCreateAccountEditText.requestFocus();
                            lastnameCreateAccountEditText.requestFocus();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }else{
                    CallCreateAccountAPI api = new CallCreateAccountAPI();
                    api.setEmail(getEmail);
                    api.setFirstName(getFirstname);
                    api.setLastName(getLastname);
                    api.setPassword(getPassword);
                    api.setConfirmPassword(getConfirmPassword);
                    api.setPhone(getPhone);
                    api.execute();

                }

            }
        });
        cancelCreateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent;
//                intent = new Intent(this,LoginActivity.class);
//                startActivity(intent);
                finish();
            }
        });
    }

    private class CallCreateAccountAPI extends AsyncTask<Void,Void,HucksonAPIResult> {
        private String mEmail;
        private String mFirstName;
        private String mLastName;
        private String mPassword;
        private String mConfirmPassword;
        private String mPhone;
        public void setEmail(String email) {
            mEmail = email;
        }
        public void setFirstName(String firstName) {
            mFirstName = firstName;
        }
        public void setLastName(String lastName) {
            mLastName = lastName;
        }
        public void setPassword(String password) {
            mPassword = password;
        }
        public void setConfirmPassword(String confirmPassword) {
            mConfirmPassword = confirmPassword;
        }
        public void setPhone(String phone) {
            mPhone = phone;
        }
        @Override
        protected HucksonAPIResult doInBackground(Void... params) {
            return new HucksonAPI().createAccount(mEmail, mFirstName, mLastName,
                    mPassword, mConfirmPassword, mPhone);
        }
        @Override
        protected void onPostExecute(HucksonAPIResult result) {
            if (result.getmStatus() == 1) {
                AppVariable.sUserID = Integer.parseInt(result.getmData());
                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getBaseContext(), "Test", Toast.LENGTH_LONG).show();
            }
        }
    }
}
