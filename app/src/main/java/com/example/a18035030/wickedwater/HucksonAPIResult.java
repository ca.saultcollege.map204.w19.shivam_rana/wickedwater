package com.example.a18035030.wickedwater;

import java.util.ArrayList;

public class HucksonAPIResult {
    private int mStatus;

    public int getmStatus() {
        return mStatus;
    }

    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public String getmError() {
        return mError;
    }

    public void setmError(String mError) {
        this.mError = mError;
    }

    public String getmData() {
        return mData;
    }

    public void setmData(String mData) {
        this.mData = mData;
    }

    public ArrayList<String> getmDataList() {
        return mDataList;
    }

    public void setmDataList(ArrayList<String> mDataList) {
        this.mDataList = mDataList;
    }

    private String mError;
    private String mData;
    private ArrayList<String> mDataList;
    public HucksonAPIResult() {
        mStatus = 0;
        mError = "";
        mData = "";
        mDataList = new ArrayList<>();
    }
    public HucksonAPIResult(int status, String error) {
        mStatus = status;
        mError = error;
        mData = "";
        mDataList = new ArrayList<>();
    }
    public HucksonAPIResult(int status, String error, String data, ArrayList<String> dataList) {
        mStatus = status;
        mError = error;
        mData = data;
        mDataList = dataList;
    }
}
