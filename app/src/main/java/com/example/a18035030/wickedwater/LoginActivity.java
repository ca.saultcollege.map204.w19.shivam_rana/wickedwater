package com.example.a18035030.wickedwater;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    EditText emailEditText,passwordEditText;
    Button loginButton,forgetPasswordButton,createAccountButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        loginButton = (Button) findViewById(R.id.loginButton);
        forgetPasswordButton = (Button) findViewById(R.id.forgetButton);
        createAccountButton  = (Button) findViewById(R.id.createAccountButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getEmailEditText = emailEditText.getText().toString();
                String getPasswordEditText = passwordEditText.getText().toString();

                if (getPasswordEditText.equals("") || getEmailEditText.equals("")) {
                    Log.d("Test", "Alert ");
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("Please enter Email and Password");
                    builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            emailEditText.requestFocus();
                            System.out.print("Alert working");
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();



                } else {
                    CallAPI api = new CallAPI();
                    api.setEmail(getEmailEditText);
                    api.setPassword(getPasswordEditText);
                    api.execute();


                }


            }
        });
        forgetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // CallAPI.CallForgotPasswordAPI api = new CallAPI.CallForgotPasswordAPI();
//                api.setEmail(emailEditText.getText().toString());
//                api.execute();

            }
        });
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CreateAccountActivity.class);
                startActivity(intent);

            }
        });
    }
    private class CallAPI extends AsyncTask<Void,Void,HucksonAPIResult> {
        private String mEmail;
        private String mPassword;
        public void setEmail(String email) {
            mEmail = email;
        }
        public void setPassword(String password) {
            mPassword = password;
        }
        @Override
        protected HucksonAPIResult doInBackground(Void... params) {
            return new HucksonAPI().auth(mEmail, mPassword);
        }
        @Override
        protected void onPostExecute(HucksonAPIResult result) {
            if (result.getmStatus() == 1) {
                AppVariable.sUserID = Integer.parseInt(result.getmData());
                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intent);
            } else {
                //show an error message
            }
        }
        private class CallForgotPasswordAPI extends AsyncTask<Void,Void,HucksonAPIResult> {
            private String mEmail;
            public void setEmail(String email) {
                mEmail = email;
            }
            @Override
            protected HucksonAPIResult doInBackground(Void... params) {
                return new HucksonAPI().forgotPassword(mEmail);
            }
            @Override
            protected void onPostExecute(HucksonAPIResult result) {
                if (result.getmStatus() == 1) {
                    //show message to check email
                } else {
                    //show message that there was an error
                }
            }
        }

    }
}
