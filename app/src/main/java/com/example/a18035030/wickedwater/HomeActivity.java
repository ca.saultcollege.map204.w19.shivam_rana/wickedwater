package com.example.a18035030.wickedwater;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import android.os.Handler;

public class HomeActivity extends AppCompatActivity {
    TextView totalViewTextView;
    ImageView advertisementImageView;
    ArrayList<String> mAds = new ArrayList<>();
    private int mCurrentAd = 0;
    private final int interval = 3000; // 3 Seconds
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable(){
        public void run() {
            nextAd();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        totalViewTextView =(TextView) findViewById(R.id.totalPoints);
        advertisementImageView = (ImageView) findViewById(R.id.advertisementtImageView);
        new CallPointSummaryAPI().execute();
        new CallActiveAdsAPI().execute();
        handler.postAtTime(runnable, System.currentTimeMillis()+interval);
    }
    private class CallPointSummaryAPI extends AsyncTask<Void,Void,HucksonAPIResult> {
        @Override
        protected HucksonAPIResult doInBackground(Void... params) {
            return new HucksonAPI().pointSummary(AppVariable.sUserID);
        }
        @Override
        protected void onPostExecute(HucksonAPIResult result) {
            if (result.getmStatus() == 1) {
                totalViewTextView.setText(result.getmData());
            } else {
                //alert the user there was an error
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getBaseContext());

                // set dialog message
                alertDialogBuilder.setMessage("There is an error");
                alertDialogBuilder.show();

            }
        }
    }
    private class CallActiveAdsAPI extends AsyncTask<Void,Void,HucksonAPIResult> {
        @Override
        protected HucksonAPIResult doInBackground(Void... params) {
            return new HucksonAPI().activeAds();
        }
        @Override
        protected void onPostExecute(HucksonAPIResult result) {
            if (result.getmStatus() == 1) {
                mAds = result.getmDataList();
                nextAd();
            } else {
                //alert the user there was an error
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getBaseContext());
                // set dialog message
                alertDialogBuilder.setMessage("There is an error");
                alertDialogBuilder.show();

            }
        }
    }
    private class DownloadImageFromInternet extends AsyncTask<String, Void, Bitmap> {
        ImageView mImageView;
        public DownloadImageFromInternet(ImageView imageView) {
            this.mImageView = imageView;
        }
        protected Bitmap doInBackground(String... urls) {
            String imageURL = urls[0];
            Bitmap bimage = null;
            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                bimage = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error Message", e.getMessage());
                e.printStackTrace();
            }
            return bimage;
        }
        protected void onPostExecute(Bitmap result) {
            mImageView.setImageBitmap(result);
            handler.postDelayed(runnable, interval);
        }
    }

    private void nextAd() {
        if ( mAds.size() > 1) {
            if ( (mCurrentAd + 1) == mAds.size() ) {
                mCurrentAd = 0;
            } else {
                mCurrentAd++;
            }
        }
        loadAd();
    }
    private void loadAd() {
        new DownloadImageFromInternet(advertisementImageView).execute(mAds.get(mCurrentAd));
    }

}


