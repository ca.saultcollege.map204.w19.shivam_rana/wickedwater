package com.example.a18035030.wickedwater;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class HucksonAPI {
    private final String TAG = "HucksonAPI";
    private static final String AUTH_METHOD = "/auth/v1/";
    private static final String API_URL = "https://huckson.digitalgrounds.ca/api";
    private static final String FORGOT_PASSWORD_METHOD = "/forgotpassword/v1/";
    private static final String CREATE_ACCOUNT_METHOD = "/createaccount/v1/";
    private static final String POINTS_SUMMARY_METHOD = "/pointsummary/v1/";
    private static final String ACTIVE_ADS_METHOD = "/activeads/v1/";


    public byte[] getUrlBytes(String urlSpec, HashMap<String, String> params) throws
            IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            String paramsString = buildParamData(params);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(paramsString);
            wr.flush();
            wr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec, HashMap<String, String> params) throws
            IOException {
        return new String(getUrlBytes(urlSpec, params));
    }

    private HucksonAPIResult callAPIForResult(String method, HashMap<String, String> params) {
        StringBuilder sb = new StringBuilder();
        sb.append(API_URL);
        sb.append(method);
        HucksonAPIResult result = new HucksonAPIResult();
        try {
            String jsonString = getUrlString(sb.toString(), params);
            Log.i(TAG, "Received JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseItems(result, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }
        return result;
    }
    private void parseItems(HucksonAPIResult result, JSONObject jsonBody)
            throws IOException, JSONException {
        JSONArray dataListJsonArray = jsonBody.getJSONArray("dataList");
        for (int i = 0; i < dataListJsonArray.length(); i++) {
            result.getmDataList().add(dataListJsonArray.getString(i));
        }
        result.setmStatus(jsonBody.getInt("status"));
        result.setmData(jsonBody.getString("data"));
        result.setmError(jsonBody.getString("error"));
    }
    private String buildParamData(HashMap<String, String> params) {
        StringBuilder sbParams = new StringBuilder();
        int i = 0;
        for (String key : params.keySet()) {
            try {
                if (i != 0){
                    sbParams.append("&");
                }
                sbParams.append(key).append("=")
                        .append(URLEncoder.encode(params.get(key), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }
        return sbParams.toString();
    }
    public HucksonAPIResult auth(String email, String password) {
        HashMap<String, String> params = new HashMap<>();
        params.put("e", email);
        params.put("p", password);
        HucksonAPIResult result = callAPIForResult(AUTH_METHOD, params);
        return result;
    }
    public HucksonAPIResult forgotPassword(String email) {
        HashMap<String, String> params = new HashMap<>();
        params.put("e", email);
        HucksonAPIResult result = callAPIForResult(FORGOT_PASSWORD_METHOD, params);
        return result;
    }
    public HucksonAPIResult createAccount(String email, String firstName, String lastName,
                                          String password, String confirmPassword, String phone) {
        HashMap<String, String> params = new HashMap<>();
        params.put("e", email);
        params.put("f", firstName);
        params.put("l", lastName);
        params.put("p", password);
        params.put("cp", confirmPassword);
        params.put("ph", phone);
        HucksonAPIResult result = callAPIForResult(CREATE_ACCOUNT_METHOD, params);
        return result;
    }
    public HucksonAPIResult pointSummary(int userID) {
        HashMap<String, String> params = new HashMap<>();
        params.put("u", Integer.toString(userID));
        HucksonAPIResult result = callAPIForResult(POINTS_SUMMARY_METHOD, params);
        return result;
    }
    public HucksonAPIResult activeAds() {
        HashMap<String, String> params = new HashMap<>();
        HucksonAPIResult result = callAPIForResult(ACTIVE_ADS_METHOD, params);
        return result;
    }




}
